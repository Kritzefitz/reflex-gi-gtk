# 0.2.0.1

* Add support for GHC 9.6.

* Update Reflex version to 0.9.

# 0.2.0.0

* Initial release
