-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

module Reflex.GI.Gtk.Widget.Ord
  ( OrdWidget(..)
  ) where

import Data.Coerce (coerce)
import Data.Function (on)
import Data.GI.Base ( GObject
                    , ManagedPtr(managedForeignPtr)
                    )
import Foreign.ForeignPtr (ForeignPtr)

newtype OrdWidget w = OrdWidget
  { fromOrdWidget :: w
  }
  deriving (Eq)

foreignPtrFromWidget :: (GObject w) => w -> ForeignPtr ()
foreignPtrFromWidget = managedForeignPtr . coerce

foreignPtrFromOrdWidget :: (GObject w) => OrdWidget w -> ForeignPtr ()
foreignPtrFromOrdWidget (OrdWidget w) = foreignPtrFromWidget w

instance (GObject w, Eq w) => Ord (OrdWidget w) where
  compare = compare `on` foreignPtrFromOrdWidget
  (<) = (<) `on` foreignPtrFromOrdWidget
  (<=) = (<=) `on` foreignPtrFromOrdWidget
  (>) = (>) `on` foreignPtrFromOrdWidget
  (>=) = (>=) `on` foreignPtrFromOrdWidget
