-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-|
Description : Run IO actions in GTK context in the face of threading
Copyright   : Sven Bartscher 2020
License     : MPL-2.0
Maintainer  : sven.bartscher@weltraumschlangen.de
Stability   : experimental

This module provides a monadic context for predictably executing 'IO'
actions that need acces to the GTK context (basically everything from
'GI.Gtk' and similar) in situations where code is executed in a
different thread than the GTK thread.
-}
module Reflex.GI.Gtk.Run
  ( module Reflex.GI.Gtk.Run.Class
  ) where

import Reflex.GI.Gtk.Run.Class
